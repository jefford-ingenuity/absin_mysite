"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.conf.urls import patterns, include, url
from views import math,hello,current_datetime,offset_Time,add,subtract, multiply, square, display_meta, contact, thanks
from views import login
from books import urls

admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', math),
    url(r'^hello/$', hello),
    url(r'^time/$', current_datetime),
    url(r'^time/offset/(\d+)/$', offset_Time),
    url(r'^add/(\d+)/(\d+)/$', add),
    url(r'^subtract/(\d+)/(\d+)/$', subtract),
    url(r'(\d+)/(\d+)/$', multiply),
    url(r'(\d+)/$', square),
    url(r'^meta/$', display_meta),
    url(r'^contact/$', contact),
    url(r'^thanks/$', thanks),
    url(r'^books/', include(urls)),
    url(r'^login/$', login),
    url(r'^admin/', include(admin.site.urls)),
)

