import datetime
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate

from books.forms import ContactForm, LogInForm, RegistrationForm
# Create your views here.

def current_datetime(request):
  now = datetime.datetime.now()
  return render(request, 'curent_datetime.html', {'current_date': now})

def offset_Time(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(hours=offset)
    return render(request, 'hours_ahead.html', {'hour_offset': offset, 'next_time': dt})
    return HttpResponse(html)

def math(request):
    return HttpResponse("Welcome to math")

def hello(request):
    return HttpResponse("Hello world")

def add(request, num1, num2):
    num1 = int(num1)
    num2 = int(num2)
    sumof = num1 + num2
    html = "The sum of %s and %s is %s" %  (num1, num2, sumof)
    return HttpResponse(html)

def subtract(request, num1, num2):
    num1 = int(num1)
    num2 = int(num2)
    diff = num1-num2
    html = "The difference of %s and %s is %s" %  (num1, num2, diff)
    return HttpResponse(html)

def multiply(request, num1, num2):
    num1 = int(num1)
    num2 = int(num2)
    prod = num1*num2
    html = "The product of %s and %s is %s" %  (num1, num2, prod)
    return HttpResponse(html)

def square(request, num1):
    num1 = int(num1)
    sqr = num1**2
    html = "The square of %s is %s" %  (num1, sqr)
    return HttpResponse(html)

def display_meta(request):
    values = request.META.items()
    values.sort()
    html = []
    for k, v in values:
        html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))
    return HttpResponse('<table>%s</table>' % '\n'.join(html))

def contact(request):
    if request.method == 'POST':  # If the form has been submitted...
        form = ContactForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            subject = form.cleaned_data.get('subject')
            message = form.cleaned_data.get('message')
            print 'SUCCESS'
            return HttpResponseRedirect('/thanks/')
    else:
        form = ContactForm()  # An unbound form

    return render(
        request,
        'contact.html',
        {'form': form}
        )
def thanks(request):
    return HttpResponse('Pak you')

def login(request):
    if request.method == 'POST':
        form = LogInForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            au = authenticate(username=uname,password=password)
            if au:
                return HttpResponseRedirect('/thanks/')
            else:
                return HttpResponse('/register/')
    else:
        form = LogInForm()
    return render(
        request,
        'login.html',
        {'form':form}
        )