from django.shortcuts import render
from django.http import HttpResponse, Http404

from .models import Book

def search(request):
  errors = []
  if 'q' in request.GET:
    q = request.GET['q']
    if not q:
        errors.append('Please input a search term')
    elif len(q) > 20:
        errors.append('search term should be less than 20 characters')
    else:
        books = Book.objects.filter(title__icontains=q)
        return render(
            request,
            'search_results.html',
            {'books': books, 'query': q},
        )
  return render(request, 'search_form.html', {'error': errors})

def index(request):
    books = Book.objects.all()
    return render(request, 'books/books.html', {'books': books})


def detail(request, pk):
    try:
        book = Book.objects.get(pk=pk)
    except Book.DoesNotExist:
        raise Http404()

    return render(request, 'books/book.html', {'book': book})