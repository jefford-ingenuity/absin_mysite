from django.conf.urls import patterns, url
from books.views import search, index, detail

urlpatterns = patterns('',
    url(r'^$', index, name='index'),
    url(r'^(\d+)/detail/$', detail, name='detail'),
    url(r'^search/$', search)
)